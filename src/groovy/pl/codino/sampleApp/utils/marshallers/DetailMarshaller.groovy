package pl.codino.sampleApp.utils.marshallers

import grails.converters.JSON
import pl.codino.sampleApp.user.Detail

class DetailMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(Detail) { Detail detail ->
            Map<String, String> map = [:]
            map['id'] = detail.id
            map['created'] = detail.dateCreated
            map['key'] = detail.key
            map['value'] = detail.value
            return map
        }
    }
}