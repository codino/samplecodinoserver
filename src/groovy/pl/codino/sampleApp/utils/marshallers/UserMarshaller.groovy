package pl.codino.sampleApp.utils.marshallers

import grails.converters.JSON
import pl.codino.sampleApp.user.User

class UserMarshaller implements Marshaller {

    @Override
    void register() {
        JSON.registerObjectMarshaller(User) { User user ->
            Map<String, String> map = [:]
            map['id'] = user.id
            map['created'] = user.dateCreated
            map['username'] = user.username
            map['email'] = user.email
            return map
        }
    }
}
