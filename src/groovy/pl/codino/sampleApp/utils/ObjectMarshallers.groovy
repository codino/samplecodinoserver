package pl.codino.sampleApp.utils

import pl.codino.sampleApp.utils.marshallers.Marshaller

class ObjectMarshallers {

    List<Marshaller> marshallers = []

    void register() {
        marshallers.each { it.register() }
    }
}
