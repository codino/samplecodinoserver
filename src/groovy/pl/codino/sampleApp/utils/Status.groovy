package pl.codino.sampleApp.utils

class Status {
    String status
    String errorMsg
    Long objectId

    private static Status success = new Status(status: "success");

    public static Status getSuccess() {
        return success;
    }

    public static Status getSuccess(Long id) {
        return new Status(status: "success", objectId: id)
    }

    public static Status getError() {
        return getErrorWithMessage("unknown error")
    }

    public static Status getError(Object domainClass) {
        String errorMsg = "Incorrect data"
        if(domainClass.hasErrors()) {
            errorMsg = "Invalid value (" + domainClass.errors.getFieldError().rejectedValue + ") in field: " + domainClass.errors.getFieldError().field
        }
        return new Status(status: "error", errorMsg: errorMsg)
    }

    public static Status getErrorWithMessage(String message) {
        return new Status(status: "error", errorMsg: message)
    }
}
