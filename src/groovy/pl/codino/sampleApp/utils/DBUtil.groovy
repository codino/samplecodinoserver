package pl.codino.sampleApp.utils

import org.apache.commons.collections.CollectionUtils
import pl.codino.sampleApp.auth.Role
import pl.codino.sampleApp.auth.UserRole
import pl.codino.sampleApp.user.User

class DBUtil {

    public void initDB() {
        if (isFirstRun()) {
            initInserts()
        }
    }

    public Boolean isFirstRun() {
        return CollectionUtils.isEmpty(Role.list())
    }

    private void initInserts() {
        Role adminRole = new Role(authority: Role.ROLE_ADMIN).save(flush: true)
        Role userRole = new Role(authority: Role.ROLE_USER).save(flush: true)

        User admin = new User(username: 'admin', password: 'password', email: 'kontakt@codino.pl').save(flush: true)
        User user1 = new User(username: 'user1', password: 'password', email: 'test1@codino.pl').save(flush: true)
        User user2 = new User(username: 'user2', password: 'password', email: 'test2@codino.pl').save(flush: true)
        User user3 = new User(username: 'user3', password: 'password', email: 'test3@codino.pl').save(flush: true)
        User user4 = new User(username: 'user4', password: 'password', email: 'test4@codino.pl').save(flush: true)
        User user5 = new User(username: 'user5', password: 'password', email: 'test5@codino.pl').save(flush: true)

        UserRole.create(admin, adminRole)
        UserRole.create(admin, userRole)
        UserRole.create(user1, userRole)
        UserRole.create(user2, userRole)
        UserRole.create(user3, userRole)
        UserRole.create(user4, userRole)
        UserRole.create(user5, userRole)
    }
}
