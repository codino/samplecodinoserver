class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(redirect:"/index.html")

        "/api/users"(controller: "users", parseRequest: true) {action = [POST:"registerUser"]}
        "/auth/api/users"(controller: "users", parseRequest: true) {action = [GET:"getAllUsers"]}
        "/auth/api/users/$userId"(controller: "users", parseRequest: true) {action = [GET:"getUser", POST:"updateUser"]}
        "/auth/api/users/$userId/detail/$detailId?"(controller: "details", parseRequest: true) {action = [POST:"saveDetail", DELETE: "deleteDetail"]}
	}
}
