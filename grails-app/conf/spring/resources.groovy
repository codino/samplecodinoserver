import pl.codino.sampleApp.utils.ObjectMarshallers
import pl.codino.sampleApp.utils.marshallers.DetailMarshaller
import pl.codino.sampleApp.utils.marshallers.UserMarshaller

beans = {
    objectMarshallers(ObjectMarshallers) {
        marshallers = [
                new UserMarshaller(),
                new DetailMarshaller()
        ]
    }
}
