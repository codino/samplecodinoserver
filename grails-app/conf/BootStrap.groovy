import org.springframework.web.context.support.WebApplicationContextUtils
import pl.codino.sampleApp.utils.DBUtil

class BootStrap {

    def init = { servletContext ->
        new DBUtil().initDB()
        def springContext = WebApplicationContextUtils.getWebApplicationContext(servletContext)
        springContext.getBean( "objectMarshallers" ).register()
    }
    def destroy = {
    }
}
