package pl.codino.sampleApp.user

import org.codehaus.groovy.grails.web.json.JSONObject
import pl.codino.sampleApp.auth.UserRole

class User {
    transient springSecurityService

    Long id
    Date dateCreated
    Date lastUpdated

    boolean enabled = true;
    boolean accountExpired;
    boolean accountLocked
    boolean passwordExpired

    String username
    String email
    String password

    static hasMany = [details: Detail]

    static constraints = {
        username unique: true, size: 4..16
        email email: true, maxSize: 100
        password blank: false, nullable: false
    }

    User fillData(JSONObject userData) {
        email = userData.email
        username = userData.username
        password = userData.password
        return this
    }

    Set<User> getAuthorities() {
        UserRole.findAllByUser(this).collect { it.role } as Set
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService?.encodePassword(password)
    }
}
