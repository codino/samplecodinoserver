package pl.codino.sampleApp.user

import org.codehaus.groovy.grails.web.json.JSONObject

class Detail {

    Long id
    Date dateCreated
    Date lastUpdated

    String key
    String value

    static belongsTo = [user: User]

    static constraints = {
        value blank: true, nullable: true
    }

    Detail fillData(JSONObject detailJson) {
        key = detailJson.key ?: 'none'
        value = detailJson.value ?: ''
        return this
    }
}
