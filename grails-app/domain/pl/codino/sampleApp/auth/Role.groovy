package pl.codino.sampleApp.auth

class Role {
    public static final String ROLE_ADMIN = 'ROLE_ADMIN';
    public static final String ROLE_USER = 'ROLE_USER';

    String authority

    static constraints = {
        authority unique: true
    }
}
