package pl.codino.sampleApp.auth

import pl.codino.sampleApp.user.User

class UserRole {

    User user
    Role role

    static mapping = {
        version false
    }

    static UserRole create(User user, Role role) {
        return new UserRole(user: user, role: role).save(flush: true, insert: true)
    }

    static boolean exists(long userId, long roleId) {
        UserRole.where { user == User.load(userId) && role == Role.load(roleId) }.count() > 0
    }
}
