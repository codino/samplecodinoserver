package pl.codino.sampleApp.privileges

import grails.plugin.springsecurity.SpringSecurityService
import grails.transaction.Transactional
import pl.codino.sampleApp.user.User

@Transactional
class ValidationService {

    SpringSecurityService springSecurityService

    def validateUserPermission(Long userId) {
        User user = springSecurityService.currentUser
        return user.id == userId
    }
}
