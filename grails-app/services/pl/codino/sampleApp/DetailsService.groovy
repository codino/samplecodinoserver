package pl.codino.sampleApp

import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONObject
import pl.codino.sampleApp.user.Detail
import pl.codino.sampleApp.user.User
import pl.codino.sampleApp.utils.Status

@Transactional
class DetailsService {

    def updateDetail(Long userId, Long detailId, JSONObject detailJson) {
        User user = User.findById(userId)
        if(!user) {
            return Status.getError()
        }
        Detail detail = user.details.find { it.id == detailId }
        detail?.fillData(detailJson)
        detail?.save(flush: true)
        return Status.getSuccess(detail?.id)
    }

    def saveNewDetail(Long userId, JSONObject detailJson) {
        User user = User.findById(userId)
        if(!user) {
            return Status.getError()
        }
        Detail detail = new Detail().fillData(detailJson)
        user.addToDetails(detail)
        user.save(flush: true)
        return Status.getSuccess(detail.id)
    }

    def deleteDetail(Long userId, Long detailId) {
        User user = User.findById(userId)
        if(!user) {
            return Status.getError()
        }
        Detail detail = user.details.find { it.id == detailId }
        user.removeFromDetails(detail)
        user.save(flush: true)
        detail.delete(flush: true)
        return Status.getSuccess(detail.id)
    }
}
