package pl.codino.sampleApp

import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.security.access.prepost.PreAuthorize
import pl.codino.sampleApp.auth.Role
import pl.codino.sampleApp.auth.UserRole
import pl.codino.sampleApp.user.User
import pl.codino.sampleApp.utils.Status

@Transactional
class UsersService {

    def getUsers() {
        return User.list()
    }

    def registerUser(JSONObject userJson) {
        return updateAndSaveUser(new User(), userJson)
    }

    @PreAuthorize('@validationService.validateUserPermission(#userId)')
    def updateUser(Long userId, JSONObject userJson) {
        return updateAndSaveUser(User.findById(userId), userJson)
    }

    private Status updateAndSaveUser(User user, JSONObject userJson) {
        user.fillData(userJson)
        if (!user.validate()) {
            return Status.getError(user)
        }
        user.save(flush: true)
        Role roleUser = Role.findByAuthority(Role.ROLE_USER)
        if(!UserRole.exists(user.id, roleUser.id)) {
            UserRole.create(user, roleUser)
        }
        return Status.getSuccess(user.id)
    }

    @PreAuthorize('@validationService.validateUserPermission(#userId)')
    def getUser(Long userId) {
        return User.findById(userId)
    }
}
