package pl.codino.sampleApp

import grails.converters.JSON

class UsersController {

    UsersService usersService

    def getAllUsers() {
        render usersService.getUsers() as JSON
    }

    def getUser(String userId) {
        render usersService.getUser(Long.parseLong(userId)) as JSON
    }

    def updateUser(String userId) {
        render usersService.updateUser(Long.parseLong(userId), request.getJSON()) as JSON
    }

    def registerUser() {
        render usersService.registerUser(request.getJSON()) as JSON
    }
}
