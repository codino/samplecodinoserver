package pl.codino.sampleApp

import grails.converters.JSON

class DetailsController {

    DetailsService detailsService

    def saveDetail(String userId, String detailId) {
        if(detailId) {
            render detailsService.updateDetail(Long.parseLong(userId), Long.parseLong(detailId), request.getJSON()) as JSON
        } else {
            render detailsService.saveNewDetail(Long.parseLong(userId), request.getJSON()) as JSON
        }
    }

    def deleteDetail(String userId, String detailId) {
        render detailsService.deleteDetail(Long.parseLong(userId), Long.parseLong(detailId)) as JSON
    }
}
