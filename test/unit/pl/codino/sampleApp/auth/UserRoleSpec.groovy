package pl.codino.sampleApp.auth

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.domain.DomainClassUnitTestMixin
import pl.codino.sampleApp.user.User
import spock.lang.Specification

@TestFor(UserRole)
@Mock([User, Role, UserRole])
@TestMixin(DomainClassUnitTestMixin)
class UserRoleSpec extends Specification {

    def springSecurityServiceMock

    def setup() {
        springSecurityServiceMock = mockFor(SpringSecurityService)
        springSecurityServiceMock.demand.encodePassword { String password -> return password }
    }

    void "should create UserRole"() {
        given:
        User user = new User(username: 'username', password: 'password', email: 'email@codino.pl')
        user.springSecurityService = springSecurityServiceMock.createMock()
        user.save(flush: true)
        Role roleUser = new Role(authority: Role.ROLE_USER).save(flush: true)
        Role roleAdmin = new Role(authority: Role.ROLE_ADMIN).save(flush: true)

        when:
        UserRole.create(user, roleUser)

        then:
        UserRole.findByUserAndRole(user, roleUser)
        !UserRole.findByUserAndRole(user, roleAdmin)
    }

    void "should exist created UserRole"() {
        given:
        User user = new User(username: 'username', password: 'password', email: 'email@codino.pl')
        user.springSecurityService = springSecurityServiceMock.createMock()
        user.save(flush: true)
        Role roleUser = new Role(authority: Role.ROLE_USER).save(flush: true)
        Role roleAdmin = new Role(authority: Role.ROLE_ADMIN).save(flush: true)

        when:
        new UserRole(user: user, role: roleAdmin).save(flush: true)

        then:
        !UserRole.exists(user.id, roleUser.id)
        UserRole.exists(user.id, roleAdmin.id)
    }
}
