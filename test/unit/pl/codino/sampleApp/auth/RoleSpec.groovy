package pl.codino.sampleApp.auth

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(Role)
class RoleSpec extends Specification {

    void "should add role ROLE_USER"() {
        given:
        Role roleUser = new Role(authority: Role.ROLE_USER)

        when:
        roleUser.save()

        then:
        Role.findByAuthority("ROLE_USER")
    }

    void "should add role ROLE_ADMIN"() {
        given:
        Role roleAdmin = new Role(authority: Role.ROLE_ADMIN)

        when:
        roleAdmin.save()

        then:
        Role.findByAuthority("ROLE_ADMIN")
    }
}
