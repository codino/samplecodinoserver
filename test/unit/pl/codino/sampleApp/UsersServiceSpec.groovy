package pl.codino.sampleApp

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.codehaus.groovy.grails.web.json.JSONObject
import pl.codino.sampleApp.auth.Role
import pl.codino.sampleApp.auth.UserRole
import pl.codino.sampleApp.user.User
import pl.codino.sampleApp.utils.Status
import spock.lang.Specification

@TestFor(UsersService)
@Mock([User, Role, UserRole])
class UsersServiceSpec extends Specification {
    private static final String STATUS_SUCCESS = "success"
    private static final String STATUS_ERROR = "error"
    User user

    def setup() {
        def springSecurityServiceMock = mockFor(SpringSecurityService)
        springSecurityServiceMock.demand.encodePassword { String password -> return password }

        new Role(authority: Role.ROLE_USER).save(flush: true)
        user = new User(username: 'username', password: 'password', email: 'email@codino.pl')
        user.springSecurityService = springSecurityServiceMock.createMock()
        user.save(flush: true)
    }

    def cleanup() {
    }

    void "should get user by Id"() {

        when:
        User result = service.getUser(user.id)

        then:
        result.email == "email@codino.pl"
        result.username == "username"
        result.password == "password"
    }

    void "should register user"() {
        given:
        def request = new JSONObject([username: "newUser", password: "password", email:"email@codino.pl"])

        when:
        Status status = service.registerUser(request)

        then:
        status.status == STATUS_SUCCESS

        User registeredUser = User.findByUsername("newUser")
        registeredUser != null
        registeredUser.username == "newUser"
    }

    void "should not register user - empty password and email"() {
        given:
        def request = new JSONObject([username: "empty", password: "", email:""])

        when:
        Status status = service.registerUser(request)

        then:
        status.status == STATUS_ERROR

        User registeredUser = User.findByUsername("empty")
        registeredUser == null
    }

    void "should update user"() {
        given:
        def request = new JSONObject([username: "newUsername", password: "password", email:"email@codino.pl"])

        when:
        Status status = service.updateUser(user.id, request)

        then:
        status.status == STATUS_SUCCESS

        User updatedUser = User.findByUsername("newUsername")
        updatedUser != null
        updatedUser.username == "newUsername"

    }
}
