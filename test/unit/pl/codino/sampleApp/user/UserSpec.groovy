package pl.codino.sampleApp.user

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.domain.DomainClassUnitTestMixin
import org.codehaus.groovy.grails.web.json.JSONObject
import pl.codino.sampleApp.auth.Role
import pl.codino.sampleApp.auth.UserRole
import spock.lang.Specification

@TestFor(User)
@Mock([User, Role, UserRole])
@TestMixin(DomainClassUnitTestMixin)
class UserSpec extends Specification {

    def EMAIL = 'kontakt@codino.pl'
    def USERNAME = 'username1'
    def PASSWORD = 'pass123'
    def builder = new groovy.json.JsonBuilder()

    void "should fill data"() {
        given:
        User user = new User()
        JSONObject jsonObject = builder {
            email EMAIL
            username USERNAME
            password PASSWORD
        }

        when:
        user = user.fillData(jsonObject)

        then:
        user.email == EMAIL
        user.username == USERNAME
        user.password == PASSWORD
    }

    void "should encode password"() {
        given:
        def springSecurityServiceMock = mockFor(SpringSecurityService)
        springSecurityServiceMock.demand.encodePassword { String password -> return password }
        User user = new User(email: EMAIL, password: PASSWORD, username: USERNAME)
        user.springSecurityService = springSecurityServiceMock.createMock()

        when:
        user.save(flush: true)

        then:
        springSecurityServiceMock.verify()
    }

    void "should reject wrong mail"() {
        given:
        User user = new User()
        JSONObject jsonObject = builder {
            email "codino.pl"
            username USERNAME
            password PASSWORD
        }

        when:
        user.fillData(jsonObject)

        then:
        !user.validate()
    }

    void "should reject empty password"() {
        given:
        User user = new User()
        JSONObject jsonObject = builder {
            email EMAIL
            username USERNAME
            password ""
        }

        when:
        user = user.fillData(jsonObject)

        then:
        !user.validate()
    }

    void "should reject to short username"() {
        given:
        User user = new User()
        JSONObject jsonObject = builder {
            email EMAIL
            username "a"
            password PASSWORD
        }

        when:
        user = user.fillData(jsonObject)

        then:
        !user.validate()
    }

    void "should return all user authorities"() {
        given:
        given:
        def springSecurityServiceMock = mockFor(SpringSecurityService)
        springSecurityServiceMock.demand.encodePassword { String password -> return password }
        User user = new User(username: 'username', password: 'password', email: 'email@codino.pl')
        user.springSecurityService = springSecurityServiceMock.createMock()
        user.save(flush: true)
        Role roleUser = new Role(authority: Role.ROLE_USER).save(flush: true)
        Role roleAdmin = new Role(authority: Role.ROLE_ADMIN).save(flush: true)

        when:
        UserRole.create(user, roleUser)

        then:
        user.getAuthorities().size() == 1
        user.getAuthorities().contains(roleUser)
    }
}
