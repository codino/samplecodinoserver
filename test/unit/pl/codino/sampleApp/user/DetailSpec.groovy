package pl.codino.sampleApp.user

import grails.test.mixin.TestFor
import org.codehaus.groovy.grails.web.json.JSONObject
import spock.lang.Specification

@TestFor(Detail)
class DetailSpec extends Specification {

    def builder = new groovy.json.JsonBuilder()

    void "should fill data"() {
        given:
        def KEY = 'key1'
        def VALUE = 'value1'
        Detail detail = new Detail()
        JSONObject jsonObject = builder {
            key KEY
            value VALUE
        }

        when:
        detail.fillData(jsonObject)

        then:
        detail.key == KEY
        detail.value == VALUE
    }

    void "should fill empty data"() {
        given:
        def KEY = 'key1'
        def VALUE = 'value1'
        Detail detail = new Detail()
        JSONObject jsonObject = builder {}

        when:
        detail.fillData(jsonObject)

        then:
        detail.key == 'none'
        detail.value == ''
    }
}
