package pl.codino.sampleApp

import grails.test.mixin.TestFor
import org.codehaus.groovy.grails.web.json.JSONObject
import spock.lang.Specification

@TestFor(DetailsController)
class DetailsControllerSpec extends Specification {

    void "should save new detail"() {
        given:
        def detailsServiceMock = mockFor(DetailsService)
        detailsServiceMock.demand.saveNewDetail { Long userId, JSONObject detailJson -> return [] }
        controller.detailsService= detailsServiceMock.createMock()
        request.json = '{"key":"key1"}'

        when:
        controller.saveDetail("3", null)

        then:
        detailsServiceMock.verify()
    }

    void "should update detail"() {
        given:
        def detailsServiceMock = mockFor(DetailsService)
        detailsServiceMock.demand.updateDetail { Long userId, Long detailId, JSONObject detailJson -> return [] }
        controller.detailsService= detailsServiceMock.createMock()
        request.json = '{"key":"key1"}'

        when:
        controller.saveDetail("3", "3")

        then:
        detailsServiceMock.verify()
    }

    void "should delete detail"() {
        given:
        def detailsServiceMock = mockFor(DetailsService)
        detailsServiceMock.demand.deleteDetail { Long userId, Long detailId -> return [] }
        controller.detailsService = detailsServiceMock.createMock()
        request.json = '{"key":"key1"}'

        when:
        controller.deleteDetail("3", "3")

        then:
        detailsServiceMock.verify()
    }
}
