package pl.codino.sampleApp.privileges

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import pl.codino.sampleApp.user.User
import spock.lang.Specification

@TestFor(ValidationService)
@Mock([User])
class ValidationServiceSpec extends Specification {

    User user

    def setup() {
        def springSecurityServiceMock = mockFor(SpringSecurityService)
        springSecurityServiceMock.demand.encodePassword { String pass -> return pass }
        user = new User(username: 'username', password: 'password', email: 'email@codino.pl')
        user.springSecurityService = springSecurityServiceMock.createMock()
        user.save(flush: true)
    }

    void "should give user permission"() {
        given:
        def springSecurityServiceMock = mockFor(SpringSecurityService)
        springSecurityServiceMock.demand.getCurrentUser { -> return user }
        service.springSecurityService = springSecurityServiceMock.createMock()

        when:
        Long userId = user.id

        then:
        service.validateUserPermission(userId)
    }

    void "should prevent user action"() {
        given:
        def springSecurityServiceMock = mockFor(SpringSecurityService)
        springSecurityServiceMock.demand.getCurrentUser { -> return user }
        service.springSecurityService = springSecurityServiceMock.createMock()

        when:
        Long userId = user.id + 1

        then:
        !service.validateUserPermission(userId)
    }
}
