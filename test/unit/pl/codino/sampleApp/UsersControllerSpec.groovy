package pl.codino.sampleApp

import grails.test.mixin.TestFor
import org.codehaus.groovy.grails.web.json.JSONObject
import spock.lang.Specification

@TestFor(UsersController)
class UsersControllerSpec extends Specification {

    void "should get all users"() {
        given:
        def usersServiceMock = mockFor(UsersService)
        usersServiceMock.demand.getUsers { -> return [] }
        controller.usersService = usersServiceMock.createMock()

        when:
        controller.getAllUsers()

        then:
        usersServiceMock.verify()
    }

    void "should register user"() {
        given:
        def usersServiceMock = mockFor(UsersService)
        usersServiceMock.demand.registerUser { JSONObject userJson -> return [] }
        controller.usersService = usersServiceMock.createMock()
        request.json = '{"username":"username"}'

        when:
        controller.registerUser()

        then:
        usersServiceMock.verify()
    }

    void "should update user"() {
        given:
        def usersServiceMock = mockFor(UsersService)
        usersServiceMock.demand.updateUser { Long userId, JSONObject userJson -> return [] }
        controller.usersService = usersServiceMock.createMock()
        request.json = '{"username":"username"}'

        when:
        controller.updateUser("3")

        then:
        usersServiceMock.verify()
    }
    void "should get user"() {
        given:
        def usersServiceMock = mockFor(UsersService)
        usersServiceMock.demand.getUser { Long userId -> return [] }
        controller.usersService = usersServiceMock.createMock()

        when:
        controller.getUser("3")

        then:
        usersServiceMock.verify()
    }
}
