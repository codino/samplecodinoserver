package pl.codino.sampleApp

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.domain.DomainClassUnitTestMixin
import org.codehaus.groovy.grails.web.json.JSONObject
import pl.codino.sampleApp.user.Detail
import pl.codino.sampleApp.user.User
import pl.codino.sampleApp.utils.Status
import spock.lang.Specification

@TestFor(DetailsService)
@Mock([User, Detail])
@TestMixin(DomainClassUnitTestMixin)
class DetailsServiceSpec extends Specification {

    public static final String KEY = "KEY"
    public static final String VALUE = "VALUE"
    public static final String SUCCESS = 'success'
    public static final String ERROR = 'error'

    def builder = new groovy.json.JsonBuilder()
    User user
    Detail detail

    def setup() {
        def springSecurityServiceMock = mockFor(SpringSecurityService)
        springSecurityServiceMock.demand.encodePassword { String password -> return password }
        user = new User(username: 'username', password: 'password', email: 'email@codino.pl')
        user.springSecurityService = springSecurityServiceMock.createMock()
        detail = new Detail(key: "key1", value: "value1")
        user.addToDetails(detail)
        user.addToDetails(new Detail(key: "key2", value: "value2"))
        user.addToDetails(new Detail(key: "key3", value: "value3"))
        user.save(flush: true)
    }

    def cleanup() {
        user.delete(flush: true)
        Detail.deleteAll(Detail.list())
    }

    void "should delete detail"() {
        when:
        Status status = service.deleteDetail(user.id, detail.id)

        then:
        status.status == SUCCESS
        user.details.size() == 2
        !user.details.contains(detail)
    }

    void "should not delete detail due to wrong user id"() {
        when:
        Status status = service.deleteDetail(user.id+1, detail.id)

        then:
        status.status == ERROR
        user.details.size() == 3
        user.details.contains(detail)
    }

    void "should not delete detail due to wrong detail id"() {
        when:
        Status status = service.deleteDetail(user.id+1, user.details.size())

        then:
        status.status == ERROR
        user.details.size() == 3
        user.details.contains(detail)
    }

    void "should save detail"() {
        given:
        JSONObject jsonObject = builder {
            key KEY
            value VALUE
        }

        when:
        Status status = service.saveNewDetail(user.id, jsonObject)

        then:
        status.status == SUCCESS
        user.details.size() == 4
        user.details.find {it.key == KEY}
    }


    void "should not save detail in wrong user"() {
        given:
        JSONObject jsonObject = builder {
            key KEY
            value VALUE
        }

        when:
        Status status = service.saveNewDetail(user.id+1, jsonObject)

        then:
        status.status == ERROR
        user.details.size() == 3
        user.details.find {it.key == KEY} == null
    }

    void "should update detail"() {
        given:
        JSONObject jsonObject = builder {
            key KEY
            value VALUE
        }

        when:
        Status status = service.updateDetail(user.id, detail.id, jsonObject)

        then:
        status.status == SUCCESS
        user.details.size() == 3
        user.details.find {it.key == KEY}
        detail.key == KEY
        detail.value == VALUE
    }

    void "should not update detail due to wrong user id"() {
        given:
        JSONObject jsonObject = builder {
            key KEY
            value VALUE
        }

        when:
        Status status = service.updateDetail(user.id + 1, detail.id, jsonObject)

        then:
        status.status == ERROR
        user.details.size() == 3
        !user.details.find {it.key == KEY}
        detail.key != KEY
        detail.value != VALUE
    }

    void "should not update detail due to wrong detail id"() {
        given:
        JSONObject jsonObject = builder {
            key KEY
            value VALUE
        }

        when:
        Status status = service.updateDetail(user.id, user.details.size()+8, jsonObject)

        then:
        status.status == SUCCESS
        user.details.size() == 3
        user.details.find {it.key == KEY} == null
    }
}
